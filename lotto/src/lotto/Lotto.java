package lotto;

import java.util.*; //Beh�vs f�r scanner
import java.io.*; //Beh�vs f�r att anv�nda File och PrintWriter

public class Lotto {

	public static void main(String[] args) throws IOException // Beh�vs f�r att
															  // Printwriter
															  // ska funka..
	{ // Inte helt s�ker p� hur det l�ser problemet dock

		Scanner input = new Scanner(System.in); // G�r s� att man kan l�sa in
												// saker (siffror i det h�r
												// fallet)

		System.out.print("V�lj dina 7 lottonummer:"); // Skriver ut en
														// textstr�ng
		int num1 = input.nextInt();
		int num2 = input.nextInt();
		int num3 = input.nextInt(); // Varje siffra man skriver med mellanslag
									// assignas till en int f�r varje 'num'
		int num4 = input.nextInt(); // som finns, dock max 7 stycken
		int num5 = input.nextInt();
		int num6 = input.nextInt();
		int num7 = input.nextInt();
		input.close(); // St�nger input
		
		// S�tter ihop en str�ng av alla siffror med mellanslag
		String nummer = (num1 + " " + num2 + " " + num3 + " " + num4 + " "
				+ num5 + " " + num6 + " " + num7);

		File file = new File(System.getProperty("java.io.tmpdir") + "lotto.txt"); // Skapar en fil
		file.getParentFile().mkdirs(); 	// Beh�vs f�r att filen ska kunna
										// skapas/hamna r�tt?
		
		PrintWriter output = new PrintWriter(file); // Visar vart str�ngen ska hamna?
		
		output.println(nummer); // Skriver ut 'nummer' till textfilen
		output.close(); // St�nger output
	}
}