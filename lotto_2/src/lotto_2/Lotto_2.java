package lotto_2;

import java.io.*; //Beh�vs f�r Scanner
import java.util.*; //Beh�vs f�r ArrayList och Collections

public class Lotto_2 {

	public static void main(String[] args) throws IOException {

		ArrayList<Integer> allaM�jliga = new ArrayList<Integer>(); //G�r en Arraylist med Integer
		Integer[] nummer = new Integer[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, //S�tter in v�rdena 1-35 i en array
										   12, 13, 14, 15, 16, 17, 18, 19, 20, 
										   21, 22, 23, 24, 25, 26, 27, 28, 29, 
										   30, 31, 32, 33, 34, 35};
		allaM�jliga.addAll(Arrays.asList(nummer)); //S�tter in siffrorna i ArrayListen s� den kan blandas
		Collections.shuffle(allaM�jliga); //Blandar alla nummer
		ArrayList<Integer> korrektTal = new ArrayList<Integer>(); //Skapar en ArrayList (f�r de vanliga r�tta numrena)
		ArrayList<Integer> korrektTal2 = new ArrayList<Integer>(); //Skapar en till ArrayList (f�r extranumrena)
		System.out.println("R�tt lottorad:"); //Skriver ut en str�ng
		
		//F�r dom f�rsta 7 numrena
		for (int i = 0; i < 7; i++) { //En loop som f�r varje int(i) upp till 7 nummer...
			korrektTal.add(allaM�jliga.get(i)); //... l�gger in tal till 'korrektTal' (fr�n allaM�jliga)
			System.out.print(allaM�jliga.get(i)); //Skriver ut talen p� sk�rmen
			System.out.print(" "); //L�gger till mellanrum mellan talen
		}
		
		System.out.print("+ "); //Skriver ut ett plustecken mellan de tidigare talen och extranummerna
		
		//F�r dom 4 extranumrena
		for (int i = 7; i < 11; i++) { //S� l�nge int (i) �r mindre �n 11 (fr�n 7) skrivs det ut tal (fyra extranummer totalt)
			korrektTal2.add(allaM�jliga.get(i)); //L�gger in dom i KorrektTal2
			System.out.print(allaM�jliga.get(i)); //Skriver ut talen p� sk�rmen
			System.out.print(" "); //L�gger till mellanrum mellan talen
		}

		System.out.println();
		System.out.println(); //Radbrytning.. G�r s�kert att g�ra p� ett finare s�tt

		Scanner scanner = new Scanner(new File(System.getProperty("java.io.tmpdir") + "\\lotto.txt")); //L�ser in lotto.txt
		int score1 = 0; //Skapar en int med namnet score1 och v�rdet 0
		int score2 = 0; //Skapar en int med namnet score1 och v�rdet 0
		
		while (scanner.hasNextInt()) //Kollar int-v�rdena i filen? Lite os�ker p� hur den funkar
		{
			int n�staNummer = scanner.nextInt(); //L�ser in int-v�rdena
			
			for (int j = 0; j < korrektTal.size(); j++) //Loopar igenom siffrorna i korrektTal och j�mf�r dom med
			{											//siffrorna i textdokumentet
				
				if (n�staNummer == korrektTal.get(j)) //Kollar om den aktuella siffran i korrektTal �r lika som 
													  //den aktuella siffran i textfilen
				{
					score1++; //L�gger till ett po�ng i score1 om siffrorna st�mmer �verens
				}
				
			}
			for (int j = 0; j < korrektTal2.size(); j++) //Samma som ovan fast f�r extrasiffrorna
			{
				
				if (n�staNummer == korrektTal2.get(j))
														
				{
					score2++;
				}
			}
		}

		scanner.close(); //St�nger scanner
		System.out.println("Antal r�tt:"); //Skriver ut en str�ng
		System.out.println(score1 + " + " + score2); //Skriver ut resultatet av j�mf�randet ovanf�r med ett plustecken mellan
	}
}